FROM ubuntu:16.04

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

RUN apt update && apt upgrade -y && apt install -qqy \
    wget \
    bzip2 \
    libssl-dev \
    openssh-server \
    nodejs \
    npm \
    graphviz

RUN npm install -g n
RUN n 9.11.1

RUN echo 'export PATH=/opt/miniconda/bin:$PATH' > /etc/profile.d/conda.sh && \
    wget --quiet https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/miniconda && \
    rm ~/miniconda.sh

RUN mkdir /var/run/sshd

RUN echo 'root:propulsion' | chpasswd

RUN sed -i '/PermitRootLogin/c\PermitRootLogin yes' /etc/ssh/sshd_config
# SSH login fix. Otherwise user is kicked off after login

RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"

RUN echo "export VISIBLE=now" >> /etc/profile

RUN mkdir -p /backend/app && \
    mkdir -p /backend/scripts && \
    mkdir -p /backend/media-files && \
    mkdir -p /backend/static-files && \
    mkdir -p /frontend


COPY ./frontend/package.json /frontend/
COPY ./frontend/package-lock.json /frontend/
COPY ./frontend /frontend/
WORKDIR /frontend
RUN npm install && npm cache clean --force
RUN npm run build 

WORKDIR /
COPY ./backend/app/requirements.yml /backend/app/requirements.yml

RUN /opt/miniconda/bin/conda env create -f /backend/app/requirements.yml
ENV PATH /opt/miniconda/envs/app/bin:$PATH

COPY ./backend/app /backend/app
COPY ./backend/scripts /backend/scripts
RUN chmod +x /backend/scripts/*

WORKDIR /backend/app

EXPOSE 8000
EXPOSE 443
EXPOSE 22
