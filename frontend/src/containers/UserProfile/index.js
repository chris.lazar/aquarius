import React, { Component } from 'react';
import { fetchUserProfile } from '../../store/actions/UserProfile'
import { connect } from 'react-redux'
import UserProfileList from '../../components/UserProfileList'

class UserProfile extends Component {

    componentWillMount() {
        console.log('In da UserProfile componentWillMount');
        console.log('user id', +this.props.match.params.userId);
        const userID = +this.props.match.params.userId;
        console.log(userID);

        this.props.dispatch(fetchUserProfile(userID))

    }

    componentDidMount() {
        console.log('In da UserProfile componentDidMount');
        const userID = +this.props.match.params.userId;
        // this.props.dispatch(fetchUserProfile(userID))
    }

    render() {
        return (
            <div>
                <UserProfileList visited_user={this.props.visited_user} />
            </div>
        )
    }
    handleClick = () => {
        console.log("in da UserProfile handleclick")
    }

}
const mapStateToProps = state => ({
    visited_user: state.visted_user
})

export default connect(mapStateToProps)(UserProfile)