import React, {Component} from "react";
import {connect} from 'react-redux';
import ButtonAppBar from '../../components/Header';
import {Button, TextField} from '@material-ui/core/';
import makeLogin from '../../store/actions/login';
import {Redirect} from "react-router";


class Login extends Component {
    state = {
        email: '',
        password: ''
    };

    handleEmail = value => {
        this.setState({email: value})
    };

    handlePassword = value => {
        this.setState({password: value})
    };

    handleClick = () => {
        this.props.dispatch(makeLogin(this.state.email, this.state.password));
        this.setState({email: '', password: ''});
    };

    render() {
        console.log('fsd', this.props.token);
        return (
            this.props.token ?

                <Redirect to="/"/>

                :
                <div>
                    <ButtonAppBar/>
                    <div className="loginfield">
                        <div>
                            <TextField
                                id="email"
                                type="text"
                                value={this.state.email}
                                onChange={(event) => this.handleEmail(event.target.value)}
                                placeholder={'email...'}
                            />
                        </div>
                        <div>
                            <TextField
                                id="password"
                                type="password"
                                value={this.state.password}
                                onChange={(event) => this.handlePassword(event.target.value)}
                                placeholder={'password...'}
                            />
                        </div>
                        <div>
                            <Button
                                onClick={this.handleClick}
                                disabled={!!localStorage.getItem('token')}
                            >Login
                            </Button>
                        </div>

                    </div>
                </div>
        )
    }
}

const mapStateToProps = state => {
    return {token: state.tokens.access}
};

export default connect(mapStateToProps)(Login)
