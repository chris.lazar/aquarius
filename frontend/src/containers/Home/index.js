import React, {Component} from 'react';
import ButtonAppBar from '../../components/Header'
import './index.css';
import {connect} from 'react-redux'
import getRestaurant from "../../store/actions/getRestaurant";
import Card from "@material-ui/core/Card/Card";
import Grid from "@material-ui/core/Grid/Grid";


class Home extends Component {

    componentDidMount() {
        console.log('In da restaurand component did mount');
        console.log('CDM---', this.props, '---CDM');
        this.props.dispatch(getRestaurant())
    }

    render() {
        console.log('hello restaurants in state(props):', this.props);
        return (
            <div className="Home">
                <ButtonAppBar/>
                <Grid container spacing={24}>

                    {this.props.restaurants.map(restaurant => <Grid item xs={3}>
                        <Card>
                            <img src={restaurant.image} alt="picture of restaurant" width="200" /><br/>
                            {restaurant.name}<br/>
                            {restaurant.city}<br/>
                            {restaurant.id}<br/>
                            </Card>
                    </Grid>)}
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {token: state.tokens.access, restaurants: state.restaurants}
};

export default connect(mapStateToProps)(Home);
