import ButtonAppBar from '../../components/Header';
import React, {Component} from "react";
import getRestaurant from '../../store/actions/getRestaurant';
import {connect} from "react-redux";
import {Redirect} from "react-router";
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';


class Restaurants extends Component {

    componentDidMount() {
        console.log('In da restaurand component did mount');
        console.log('CDM---', this.props, '---CDM');
        this.props.dispatch(getRestaurant())
    }

    render() {
        console.log('hello restaurants in state(props):', this.props);
        return (
            this.props.token ?
                <div>
                    <ButtonAppBar/>
                    <Grid container spacing={24}>

                        {this.props.restaurants.map(restaurant => <Grid item xs={3}>
                            <Card>
                            <img src={restaurant.image} alt="picture of restaurant" width="200" /><br/>
                            {restaurant.name}<br/>
                            {restaurant.city}<br/>
                            {restaurant.id}<br/>
                            </Card>

                        </Grid>)}
                    </Grid>
                </div>
                :
                <Redirect to="/login"/>
        )
    }
}

const mapStateToProps = state => {
    return {token: state.tokens.access, restaurants: state.restaurants}
};

export default connect(mapStateToProps)(Restaurants)
