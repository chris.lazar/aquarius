import React, { Component } from 'react';
import { fetchUsers } from '../../store/actions/Users'
import { connect } from 'react-redux'
import UsersList from '../../components/UsersList'

class Users extends Component {

    componentDidMount() {
        console.log('In da Users componentDidMount');
        this.props.dispatch(fetchUsers())
    }

    render() {
        return (
            <div>
                <UsersList users={this.props.users} />
            </div>
        )
    }
    handleClick = () => {
        console.log("i'm clicking in hereee")
    }

}
const mapStateToProps = state => ({
    users: state.users
})

export default connect(mapStateToProps)(Users)