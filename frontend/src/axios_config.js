import axios from "axios";

const instance = axios.create({
  baseURL: "http://localhost:8000/"
  //headers.common['Authorization'] = token   <----we can use this if we want... Global axios defaults
});

export default instance;


