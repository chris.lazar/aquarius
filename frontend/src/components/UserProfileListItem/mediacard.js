import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import ContainedButtons from '../Buttons/ShowReviewsShowComments.js'
import './mediacard.css'

const styles = {
    card: {
        maxWidth: 700,
    },
    media: {
        height: 340,
    },
};



function MediaCard(props) {
    const { classes } = props;
    console.log("Visited user media card", props);
    console.log('Show User Profile here', props.visited_user.user_profile);
    if (props.visited_user.user_profile===undefined){
        return(<div>Wait a sec</div>)
    }
    return (
        <Card className={classes.card}>
            <CardActionArea onClick={handleClick}>
            <h1 align='center'>User Profile</h1>
                <CardMedia
                    className={classes.media}
                    image={props.visited_user.user_profile.profile_picture}
                />
            </CardActionArea>
            <CardContent>
                <CardActionArea onClick={handleClick}>
                    <Typography gutterBottom variant="h5" component="h2">
                        {props.visited_user.first_name} {props.visited_user.last_name}
                    </Typography>
                </CardActionArea>
                <Typography component="p">
                    {props.visited_user.user_profile.description}
                </Typography>
            </CardContent>
            <div className='Centerer'>
            <ContainedButtons/>
            </div>
        </Card>
    );
}
const handleClick = () => {
    console.log('In da handleclick of the Userprofile card');
    
}

MediaCard.propTypes = {
    classes: PropTypes.object.isRequired,
    visited_user: PropTypes.object.isRequired,
    // user_profile: PropTypes.object.isRequired
};

export default withStyles(styles)(MediaCard);