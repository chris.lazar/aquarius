import React, { Component } from 'react';
import { connect } from 'react-redux';
import MediaCard from '../UserProfileListItem/mediacard.js'
import ButtonAppBar from '../Header'
import './mediacard.css'

class UserProfileList extends Component {
  render() {
    console.log(this.props.users);
    console.log("User profile list visited user", this.props.visited_user);

    return (
      <div>
        <ButtonAppBar />
        <div className='Centerer'>
          <MediaCard visited_user={this.props.visited_user}></MediaCard>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    visited_user: state.visited_user
  }
}

export default connect(mapStateToProps)(UserProfileList)
