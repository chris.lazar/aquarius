import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import ContainedButtons from '../Buttons/ShowReviewsShowComments.js'

const styles = {
    card: {
        maxWidth: 345,
    },
    media: {
        height: 140,
    },
};

function MediaCard(props) {
    const { classes } = props;
    return (
        <Card className={classes.card}>
            <CardActionArea onClick={handleClick}>
                <CardMedia
                    className={classes.media}
                    image={props.user.user_profile.profile_picture}
                />
            </CardActionArea>
            <CardContent>
                <CardActionArea onClick={handleClick}>
                    <Typography gutterBottom variant="h5" component="h2">
                        {props.user.username}
                    </Typography>
                </CardActionArea>
                <Typography component="p">
                    {props.user.user_profile.description}
                </Typography>
            </CardContent>
            <ContainedButtons />
        </Card>
    );
}
const handleClick = () => {
    console.log('In da handleclick of the card');
}

MediaCard.propTypes = {
    classes: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired
};

export default withStyles(styles)(MediaCard);