import Grid from '@material-ui/core/Grid';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import MediaCard from '../UserListItem/mediacard.js'
import ButtonAppBar from '../Header'



class UserList extends Component {
  render() {
    console.log(this.props.users);

    return (
    <div>
      <ButtonAppBar />
      <h1 align='center'>Users</h1>
    <Grid
      style={{
        marginTop: "25px",
        marginRight: "auto",
        marginLeft: "auto",
        maxWidth: "1200px",
      }}
      container
      spacing={24}
    >
      {
        this.props.users.map(user =>
          <Grid item xs={3} key={user.id}>
            <MediaCard user={user}></MediaCard>
          </Grid>
        )
      }
    </Grid>
    </div>
    )
  }


}

const mapStateToProps = state => {
  return {
    users: state.users
  }
}

export default connect(mapStateToProps)(UserList)
