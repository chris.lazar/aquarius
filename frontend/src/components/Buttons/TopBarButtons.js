import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import './TopBarButtons.css'
import {withRouter} from "react-router";
import {Link} from 'react-router-dom'

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
});

function TopBarButtons(props) {
    const {classes} = props;
    return (
        <div className='AlignEnd'>
            <div>
                <h3 style={{fontFamily: "Comic Sans MS"}} className={classes.button}>
                    Luna
                </h3>
            </div>
            <div>
                <Link to='/' style={{textDecoration: 'none'}}>
                    <Button style={{fontFamily: "Comic Sans MS"}} color="secondary" className={classes.button}>
                        <b>Home</b>
                    </Button>
                </Link>
                <Link to='/users/' style={{textDecoration: 'none'}}>
                <Button style={{fontFamily: "Comic Sans MS"}} color="secondary" className={classes.button}>
                    <b>Search</b>
                </Button>
                </Link>
                <Link to='/userprofile/1/' style={{textDecoration: 'none'}}>
                    <Button style={{fontFamily: "Comic Sans MS"}} color="secondary" className={classes.button}>
                        <b>Profile</b>
                    </Button>
                </Link>
                <Button style={{fontFamily: "Comic Sans MS"}} color="secondary" href="#text-buttons"
                        className={classes.button}>
                    <b>Sign Up</b>
                </Button>
                <Link to='/login/' style={{textDecoration: 'none'}}>
                    <Button style={{fontFamily: "Comic Sans MS"}} color="secondary" href="#text-buttons"
                            className={classes.button}>
                        <b>Login</b>
                    </Button>
                </Link>
            </div>
        </div>
    );
}

TopBarButtons.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(TopBarButtons));