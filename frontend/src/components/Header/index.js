import React from "react";
import PropTypes from "prop-types";
import {withStyles} from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import {withRouter} from 'react-router-dom';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import TopBarButtons from '../Buttons/TopBarButtons.js'

import MenuIcon from "@material-ui/icons/Menu";
import toRenderProps from 'recompose/toRenderProps';
import withState from 'recompose/withState';


const styles = {
    root: {
        flexGrow: 1
    },
    grow: {
        flexGrow: 1
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20
    }
};

const WithState = toRenderProps(withState('anchorEl', 'updateAnchorEl', null));


function ButtonAppBar(props) {
    const {classes} = props;
    return (
        <WithState>
            {({anchorEl, updateAnchorEl}) => {
                const open = Boolean(anchorEl);
                const handleClose = () => {
                    updateAnchorEl(null);
                };
                const toRestaurants = () => {
                    props.history.push('/restaurants/');
                    updateAnchorEl(null);
                };
                const toHome = () => {
                    props.history.push('/');
                    updateAnchorEl(null);
                };
                const toUsers = () => {
                    props.history.push('/users/');
                    updateAnchorEl(null);
                };


                return (
                    <div className={classes.root}>
                        <AppBar position="static">
                            <Toolbar>
                                <IconButton
                                    className={classes.menuButton}
                                    color="inherit"
                                    aria-label="Menu"
                                    aria-owns={open ? 'render-props-menu' : undefined}
                                    aria-haspopup="true"
                                    onClick={event => {
                                        updateAnchorEl(event.currentTarget);
                                    }}
                                >
                                    <MenuIcon/>
                                </IconButton>
                                <Menu id="render-props-menu" anchorEl={anchorEl} open={open} onClose={handleClose}>
                                    <MenuItem onClick={toHome}>Home</MenuItem>
                                    <MenuItem onClick={toRestaurants}>Restaurants</MenuItem>
                                    <MenuItem onClick={toUsers}>Users</MenuItem>
                                </Menu>
                                <Typography variant="h6" color="inherit" className={classes.grow}>
                                    <TopBarButtons />
                                </Typography>
                            </Toolbar>
                        </AppBar>
                    </div>
                );

            }}
        </WithState>
    );
}

ButtonAppBar.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withRouter(withStyles(styles)(ButtonAppBar));
