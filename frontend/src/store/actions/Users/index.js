import axios from '../../../axios_config'

export const fetchUsers = () => (dispatch, getState) => {
    console.log("In dat fetch users");

    axios.get('backend/api/users/list/')
        .then(response => { return response.data })
        .then(users => { dispatch(showUsers(users)) })
}

export const showUsers = (users) => (dispatch, getState) => {
    dispatch({
        type: 'GET_USERS',
        payload: {
            users: users
        }
    }
    )
}

