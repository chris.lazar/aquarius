import axios from "../../axios_config";


const getRestaurant = () => (dispatch, getState) => {
    axios
        .get('backend/api/restaurants/')
        .then(response => {
            console.log('response---',response.data, '---response');
            return response.data;
        })
        .then(restaurants => {
            console.log('In da get restaurants action', restaurants);
            dispatch({
                type: 'setRestaurants',
                payload: restaurants
            })
        })
};

export default getRestaurant
