import axios from '../../../axios_config'

export const fetchUserProfile = (id) => (dispatch, getState) => {
    console.log("In dat fetch user_profile");
    console.log("user profile action id", id);
    

    axios.get(`backend/api/users/${id}/`)
        .then(response => { return response.data })
        .then(user_profile => { dispatch(showUserProfile(user_profile)) })
}

export const showUserProfile = (user_profile) => (dispatch, getState) => {
    console.log("Get user action", user_profile);
    
    dispatch({
        type: 'GET_USER_PROFILE',
        payload: {
            visited_user: user_profile
        }
    }
    )
}

