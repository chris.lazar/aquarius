import axios from "../../axios_config";
import { setToken } from './setToken';

const makeLogin = (email, password) => (dispatch) => {
    console.log('reached makelogin :)');

    const data = {
        username: email,
        password: password
    };

    axios
        .post('backend/api/token/', data)
        .then(response => {
            console.log('reached response',response.data);
            return response.data;
        })
        .then(tokens => {
        dispatch(setToken(tokens));
        localStorage.setItem('access', tokens.access);
        localStorage.setItem('refresh', tokens.refresh);
        })

};

export default makeLogin

