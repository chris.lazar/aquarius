const initialState = { restaurants: [], user: {}, users: [], visited_user: {}, tokens: {} };

const reducer = (state = initialState, action) => {
    console.log('hello from the reducer', state);
    switch (action.type) {
        case 'getRestaurants': {
            const restaurants = action.payload.filter(x => x.content !== undefined);
            return { restaurants: restaurants }
        }
        case 'setToken': {
            console.log('hello, i am changing the state:)');
            return { ...state, tokens: action.payload.tokens }
                ;
        }
        case 'GET_USERS': {
            return { ...state, users: action.payload.users }
        }
        case 'GET_USER_PROFILE': {
            console.log("Reducer State", state);
            console.log("reducer payload", action.payload);

            return { ...state, visited_user: action.payload.visited_user}
        }
        case 'setRestaurants': {
            console.log('hello, i am changing the restaurants in da state:)',action.payload,'blblbl');
            return {...state, restaurants: action.payload}
            ;
        }
        default:
            return state

    }
};

export default reducer
