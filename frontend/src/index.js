import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Home from './containers/Home';
import Login from './containers/Login';
import Restaurants from './containers/Restaurants';

import Users from './containers/Users';
import UserProfile from './containers/UserProfile';
import * as serviceWorker from './serviceWorker';
import store from "./store";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { createMuiTheme } from "@material-ui/core/styles";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import { setToken } from './store/actions/setToken.js'
import orange from '@material-ui/core/colors/orange';

const theme = createMuiTheme({
  palette: {
    type: "light",
    primary: orange,
    secondary: {
      main: '#FAFAFA'
    },
  }
});

const token = localStorage.getItem("access");
const refresh = localStorage.getItem("refresh");
const tokens = {
  access: token,
  refresh: refresh
};

if (tokens.access) {
  console.log("In da store state...", store.getState());
  store.dispatch(setToken(tokens));
}

ReactDOM.render(
    <MuiThemeProvider theme={theme}>
      <Provider store={store}>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/restaurants" component={Restaurants}/>
            <Route exact path="/users/" component={Users}/>
            <Route exact path="/userprofile/:userId/" component={UserProfile}/>
          </Switch>
        </BrowserRouter>
      </Provider>
    </MuiThemeProvider>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
