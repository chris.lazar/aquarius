#!/usr/bin/env bash
set -e
/opt/miniconda/envs/app/bin/coverage run --omit=*/migrations/*,*init*,manage.py,*test* manage.py test

# Grab the coverage for Gitlab: use "TOTAL:\s+\d+\%" in the gitlab-ci-settings
LOG=$(mktemp)
/opt/miniconda/envs/app/bin/coverage report -m  2>&1 >$LOG
cat $LOG
grep TOTAL $LOG | awk '{ print "TOTAL: "$4; }'
rm $LOG