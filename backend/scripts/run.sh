#!/usr/bin/env bash
python manage.py migrate
python manage.py collectstatic --no-input
python manage.py runserver 0.0.0.0:8000

#exec /opt/miniconda/envs/app/bin/uwsgi --ini /scripts/uwsgi.ini