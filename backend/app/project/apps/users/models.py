import datetime
from random import choice
from string import ascii_uppercase

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

User = get_user_model()


class UserProfile(models.Model):
    user = models.OneToOneField(
        unique=True,
        verbose_name='user',
        related_name='user_profile',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

    location = models.CharField(
        verbose_name='location',
        max_length=25,
        blank=True
    )

    phone = models.CharField(
        verbose_name='phone',
        max_length=15,
        blank=True
    )

    thingsILove = models.TextField(
        verbose_name='thingsILove',
        max_length=100,
        blank=True
    )

    description = models.TextField(
        verbose_name='description',
        max_length=500,
        blank=True
    )

    joined = models.DateTimeField(
        verbose_name='joined',
        auto_now_add=True
    )

    profile_picture = models.URLField(
        max_length=240,
    )

    validation_code = models.CharField(
        verbose_name='validation_code',
        max_length=200,
        null=True,
        blank=True,
    )

    def generate_validation_code(self):
        self.validation_code = (''.join(choice(ascii_uppercase) for i in range(12)))
        self.save()


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.user_profile.save()


class LikeComments(models.Model):
    comment_liked_by = models.ForeignKey(
        to=UserProfile,
        related_name='comments_liked',
        blank=True,
        on_delete=models.CASCADE,
    )


class Comments(models.Model):
    content = models.TextField(
        verbose_name='content',
        max_length=1500,
        blank=False,
    )

    created = models.DateTimeField(
        editable=False,
        verbose_name='created',
        auto_now_add=True
    )

    modified = models.DateTimeField(
        editable=True,
        verbose_name='modified',
        blank=True,
        null=True
    )

    comment_likes = models.ForeignKey(
        related_name='likes_comment',
        to=LikeComments,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )

    comment_creator = models.ForeignKey(
        to=UserProfile,
        related_name='user_comments',
        blank=True,
        on_delete=models.CASCADE
    )

    belongs_to_review = models.ForeignKey(
        to='restaurants.Reviews',
        related_name='review_comments',
        blank=False,
        null=True,
        on_delete=models.CASCADE
    )

    def save(self):
        if self.id:
            self.modified = datetime.datetime.now()
        else:
            self.created = datetime.datetime.now()
        super(Comments, self).save()

# ----comment signal
# @receiver(pre_save, sender=Comments)
# def modified_comment(sender, instance, **kwargs):
#     instance.modified = timezone.now()
# instance.modified.save()
