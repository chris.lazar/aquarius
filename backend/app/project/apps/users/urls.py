from django.urls import path

from project.apps.users.views import GetUserComments, CommentOnReview, UpdateUserProfile, DeleteComment, \
    ListAllUsers, SearchUsers, GetUserProfile, RegisterView, ValidateView

urlpatterns = [
    # -----------Comments-------------
    path('review/comment/<int:pk>/', GetUserComments.as_view(), name='get-user-comments'),
    path('review/comment/new/', CommentOnReview.as_view(), name='create-a-comment'),
    path('review/comment/<int:review_pk>/', DeleteComment.as_view(), name='delete-comment'),
    # ------------Users------------
    path('me/', UpdateUserProfile.as_view(), name='get-update-user-profile.'),
    path('users/list/', ListAllUsers.as_view(), name='get-all-users.'),
    path('users/search/', SearchUsers.as_view(), name='search-for-an-user'),
    path('users/<int:pk>/', GetUserProfile.as_view(), name='getASpecificUserProfile'),
    path('registration/', RegisterView.as_view(), name='user-registration'),
    path('validation/', ValidateView.as_view(), name='user-activation'),
]
