from django.contrib.auth import get_user_model
from django.core.mail import EmailMessage
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from project.apps.users.models import UserProfile, LikeComments, Comments

User = get_user_model()


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ['id', 'user', 'location', 'phone', 'thingsILove', 'description', 'joined', 'profile_picture']
        read_only_fields = ['id', 'user', 'profile_picture']


class UserSerializer(serializers.ModelSerializer):
    user_profile = UserProfileSerializer(read_only=True)
    username = serializers.CharField(required=False)

    class Meta:
        model = User
        fields = ['id', 'user_profile',
                  'username', 'first_name', 'last_name', 'email']

        read_only_fields = ['id', 'user_profile']


class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = LikeComments
        fields = ['id', 'user', 'comment']
        read_only_fields = ['id', 'user', 'comment']

    def create(self, pk):
        return LikeComments.objects.get_or_create(
            user=self.context.get('request').user,
            post=Comments.objects.get(pk=pk),
        )

    def destroy(self, pk):
        return LikeComments.objects.delete(
            user=self.context.get('request').user,
            post=Comments.objects.get(pk=pk),
        )


class CommentSerializer(serializers.ModelSerializer):
    # likes
    # likes_count = serializers.SerializerMethodField(read_only=True)
    #
    # def get_likes_count(self, comment):
    #     return comment.comment_likes.count()

    class Meta:
        model = Comments
        # fields = ['id', 'content', 'created', 'modified', 'likes_count', 'comment_creator', 'belongs_to_review']
        # read_only_fields = ['id', 'content', 'created', 'modified', 'likes_count', 'comment_creator']
        fields = ['id', 'content', 'created', 'modified', 'comment_creator', 'belongs_to_review']
        read_only_fields = ['id', 'content', 'created', 'modified', 'comment_creator']

    def create(self, validated_data):
        return Comments.objects.create(
            comment_creator=self.context.get('request').user.user_profile,
            **validated_data,
        )


class RegistrationSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate_email(self, email):
        try:
            User.objects.get(email=email)
            raise ValidationError('Email address is already in use')
        except User.DoesNotExist:
            return email

    def send_validation_email(self, email, validation_code):
        message = EmailMessage(
            to=[email],
            subject='Verify your Motion email',
            body=f'Your code is: {validation_code}',
        )
        message.send()

    def save(self, **kwargs):
        user = User.objects.create_user(
            username=kwargs.get('email'),
            email=kwargs.get('email'),
            is_active=False
        )
        user.user_profile.generate_validation_code()
        self.send_validation_email(user.email, user.user_profile.validation_code)
        return user


class ValidationSerializer(serializers.Serializer):
    code = serializers.CharField(required=True)
    email = serializers.EmailField()
    username = serializers.CharField()
    password = serializers.CharField()
    password_repeat = serializers.CharField()

    def validate_username(self, username):
        try:
            User.objects.get(username=username)
            raise ValidationError('Username taken')
        except User.DoesNotExist:
            if '@' in username:
                raise ValidationError('Username cannot contain an "@" sign')
        return username

    def validate_email(self, email):
        try:
            return User.objects.get(email=email, is_active=False)
        except User.DoesNotExist:
            raise ValidationError('User Does Not Exist or is active')

    def validate(self, attrs):
        user = attrs.get('email')
        if user.user_profile.validation_code != attrs.get('code'):
            raise ValidationError({'code': 'Code not valid'})
        if attrs.get('password') != attrs.get('password_repeat'):
            raise ValidationError({'password_repeat': 'Passwords do not match'})
        return attrs

    def save(self, **kwargs):
        user = kwargs.get('email')
        user.username = kwargs.get('username')
        user.is_active = True
        user.set_password(kwargs.get('password'))
        user.save()
        user.user_profile.validation_code = 0
        user.user_profile.save()
        return user
