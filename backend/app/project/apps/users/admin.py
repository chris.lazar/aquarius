from django.contrib import admin

from .models import UserProfile, Comments, LikeComments

admin.site.register(UserProfile)
admin.site.register(Comments)
admin.site.register(LikeComments)
