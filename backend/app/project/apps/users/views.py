from django.contrib.auth import get_user_model
from rest_framework import filters
from rest_framework import permissions
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveUpdateDestroyAPIView, \
    GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from project.apps.users.models import Comments, UserProfile
from project.apps.users.serializers import CommentSerializer, UserProfileSerializer, UserSerializer, \
    RegistrationSerializer, \
    ValidationSerializer  # noqa

User = get_user_model()


# ------Comments---------
class GetUserComments(ListAPIView):
    serializer_class = CommentSerializer

    def get_queryset(self):
        return Comments.objects.filter(user=self.request.user).all


class CommentOnReview(CreateAPIView):
    queryset = Comments.objects.all()
    serializer_class = CommentSerializer


class DeleteComment(RetrieveUpdateDestroyAPIView):
    serializer_class = CommentSerializer

    def get_queryset(self):
        return Comments.objects.filter(user=self.request.user).all


# ------Users---------
class UpdateUserProfile(GenericAPIView):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

    def get(self, request, **kwargs):
        user = self.request.user
        return Response(self.serializer_class(instance=user.user_profile).data)

    def post(self, request, *args, **kwargs):
        user = self.request.user
        serializer = self.serializer_class(instance=user.user_profile, data=request.data)
        if serializer.is_valid(raise_exception=True):
            updated_user = serializer.save()
            return Response(self.serializer_class(updated_user).data)


class ListAllUsers(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [AllowAny]


class SearchUsers(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('username', 'first_name', 'last_name', 'email')


class GetUserProfile(ListAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [AllowAny]

    def get(self, request, **kwargs):
        user = self.get_object()
        return Response(self.serializer_class(user).data)


User = get_user_model()


class RegisterView(GenericAPIView):
    serializer_class = RegistrationSerializer
    permission_classes = [permissions.AllowAny]

    def post(self, request, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.save(**serializer.validated_data)
            return Response({'detail': f'Please use the validation link sent to {user.email} to activate your profile'})


class ValidateView(GenericAPIView):
    serializer_class = ValidationSerializer
    permission_classes = [permissions.AllowAny]

    def post(self, request, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.save(**serializer.validated_data)
            return Response({'detail': f'Registration successful for {user.email}.'
                                       f'Your account is now active.'})
