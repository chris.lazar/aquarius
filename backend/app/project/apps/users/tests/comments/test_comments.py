# from rest_framework import status
#
# from project.apps.restaurants.models import Reviews
# from project.apps.users.tests.master_tests import MasterTestWrapper
#
#
# class TestCreateComment(MasterTestWrapper.MasterTests):
#     endpoint = 'create-a-comment'
#     methods = ['POST']
#
#     def setUp(self):
#         super().setUp()
#         Reviews.objects.create(
#             owner_of_review=self.user.user_profile,
#             content="sadasd"
#         )
#
#     def test_unauthorized_access(self):
#         url = self.get_url()
#         response = self.client.post(url)
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#     def test_creating_comment(self):
#         url = self.get_url()
#         self.authorize(user=self.user)
#         data = {
#             'content': 'Lorem Ipsum 1'
#         }
#         response = self.client.post(url, data)
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED)
#         self.assertEqual(response.data.get('content'), 'Lorem Ipsum 1')
#
#     def test_creating_blank_post(self):
#         url = self.get_url()
#         self.authorize(user=self.user)
#         data = {
#             'content': ''
#         }
#         response = self.client.post(url, data)
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
