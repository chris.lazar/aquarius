# from django.contrib.auth import get_user_model
# from rest_framework import status
#
# from project.apps.users.tests.master_tests import MasterTestWrapper
#
# User = get_user_model()
#
#
# class Validation(MasterTestWrapper.BasicMasterTests):
#     endpoint = 'user-activation'
#     methods = ['POST']
#
#     def setUp(self):
#         super().setUp()
#         self.registered_user = User.objects.create_user(
#             username='test2@test.com',
#             email='test2@test.com',
#             is_active=False
#         )
#         self.body = {
#             "username": "test2@test.com",
#             "code": '12345',
#             "email": 'test2@test.com',
#
#         }
#         self.registered_user.user_profile.verification_code = 12345
#         self.registered_user.user_profile.save()
#
#     def test_validation(self):
#         url = self.get_url()
#         response = self.client.post(url, self.body)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#         self.assertEqual(response.json().get('detail'),
#                          f'Registration successful for {self.registered_user.email}.\n Your account is now active.')
#
#     def test_user_is_active(self):
#         url = self.get_url()
#         response = self.client.post(url, self.body)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#         self.assertTrue(User.objects.get(email='test2@test.com').is_active)
#
#     def test_no_code(self):
#         url = self.get_url()
#         response = self.client.post(url, self.body)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#         self.assertEqual(User.objects.get(email='test2@test.com').user_profile.verification_code, 0)
#
#     def test_wrong_code(self):
#         url = self.get_url()
#         self.body.update({
#             'verification_code': '542',
#         })
#         response = self.client.post(url, self.body)
#         # self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
#         self.assertEqual(response.json().get('detail'),
#                          f'Invalid code.')
#
#     def test_passwords_dont_match(self):
#             url = self.get_url()
#             self.body.update({
#                 'password_repeat': 'wrong',
#             })
#             response = self.client.post(url, self.body)
#             self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
#             self.assertEqual(response.json().get('password_repeat'), 'Passwords do not match')
#
#     def test_user_does_not_exist(self):
#             url = self.get_url()
#             self.body.update({
#                 'email': 'wrong@email.com',
#             })
#             response = self.client.post(url, self.body)
#             self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
#             self.assertEqual(response.json().get('email'), 'User Does Not Exist or is active')
#
#     def test_user_already_exists(self):
#         url = self.get_url()
#         self.body.update({
#             'email': 'test@test.com',
#         })
#         response = self.client.post(url, self.body)
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
#         self.assertEqual(response.json().get('email'), 'User Does Not Exist or is active')
