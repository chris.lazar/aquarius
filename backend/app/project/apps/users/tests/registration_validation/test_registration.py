from django.contrib.auth import get_user_model
from django.core import mail
from rest_framework import status

from project.apps.users.tests.master_tests import MasterTestWrapper

User = get_user_model()


class Registration(MasterTestWrapper.BasicMasterTests):
    endpoint = 'user-registration'
    methods = ['POST']
    body = {
        "email": "test2@test2.com",
        "username": "test2@test2.com",
        "password": "super_secure"
    }

    def setUp(self):
        super().setUp()

    def test_registration(self):
        url = self.get_url()
        response = self.client.post(url, self.body)
        user = User.objects.get(email="test2@test2.com")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('detail'),
                         f'Please use the validation link sent to {user.email}'
                         f' to activate your profile')

    def test_username_in_db(self):
        url = self.get_url()
        response = self.client.post(url, self.body)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(User.objects.get(email='test2@test2.com').email, 'test2@test2.com')

    def test_email_sent(self):
        url = self.get_url()
        response = self.client.post(url, self.body)
        user = User.objects.get(email='test2@test2.com')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn(str(user.user_profile.validation_code), mail.outbox[0].body)
