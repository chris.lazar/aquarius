from django.contrib import admin

from .models import Restaurant, Reviews

admin.site.register(Restaurant)
admin.site.register(Reviews)
