from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class Restaurant(models.Model):
    name = models.CharField(
        max_length=120,
    )
    category = models.CharField(
        max_length=120,
    )
    country = models.CharField(
        max_length=120,
    )
    street = models.CharField(
        max_length=120,
    )
    city = models.CharField(
        max_length=120,
    )
    zip = models.IntegerField(
        null=True
    )
    website = models.CharField(
        max_length=120,
    )
    phone = models.CharField(
        max_length=24,
    )
    email = models.EmailField(
        max_length=254,
        null=True,
    )
    opening_hours = models.CharField(
        max_length=120,
    )
    price_level = models.IntegerField(
    )
    image = models.CharField(
        max_length=300,
    )
    restaurant_creator = models.ForeignKey(
        verbose_name="Restaurant Creator",
        related_name='restaurant_created',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )


class Reviews(models.Model):
    owner_of_review = models.ForeignKey(
        related_name='reviews',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    content = models.TextField(
        max_length=2000,
    )
    created = models.DateTimeField(
        auto_now_add=True,
    )
    modified = models.DateTimeField(
        auto_now_add=True,
    )
    belongs_to_restaurant = models.ForeignKey(
        related_name='restaurant_reviews',
        to='restaurants.Restaurant',
        on_delete=models.CASCADE,
    )
    rating = models.IntegerField(
        null=True,
    )


class LikesReview(models.Model):
    review_liked_by = models.ForeignKey(
        related_name='reviews_liked',
        to='users.UserProfile',
        on_delete=models.CASCADE,
        null=True
    )
    likes_review = models.ForeignKey(
        related_name='review_likes',
        to='Reviews',
        on_delete=models.CASCADE,
        null=True
    )
