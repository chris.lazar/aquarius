from django.urls import path

from project.apps.restaurants.views import ListCreateRestaurantView, ListCreateRestaurantReviewsView

urlpatterns = [
    path('restaurants/', ListCreateRestaurantView.as_view(), name='get-restaurants'),
    path('restaurants/new/', ListCreateRestaurantView.as_view(), name='new-restaurant'),
    path('reviews/new/<int:pk>/', ListCreateRestaurantReviewsView.as_view(), name='new-review'),
    path('reviews/restaurant/<int:pk>/', ListCreateRestaurantReviewsView.as_view(), name='get-reviews')
]
