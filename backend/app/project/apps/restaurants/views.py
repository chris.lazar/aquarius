from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from project.apps.restaurants.models import Restaurant, Reviews
from project.apps.restaurants.serializers import RestaurantSerializer, ReviewsSerializer


class ListCreateRestaurantView(ListCreateAPIView):
    # model = Restaurant
    serializer_class = RestaurantSerializer
    queryset = Restaurant.objects.all()

    permission_classes = (AllowAny,)


class ListCreateRestaurantReviewsView(ListCreateAPIView):
    serializer_class = ReviewsSerializer
    queryset = Restaurant.objects.all()

    def get(self, request, **kwargs):
        restaurant = self.get_object()
        reviews = Reviews.objects.filter(belongs_to_restaurant=restaurant)
        return Response(self.serializer_class(reviews, many=True).data)
