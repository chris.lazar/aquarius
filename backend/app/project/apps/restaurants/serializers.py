from rest_framework import serializers

from project.apps.restaurants.models import Restaurant, Reviews, LikesReview


class RestaurantSerializer(serializers.ModelSerializer):
    review_count = serializers.SerializerMethodField(read_only=True)

    def get_review_count(self, restaurant):
        return restaurant.restaurant_reviews.count()

    class Meta:
        model = Restaurant
        fields = ['id', 'restaurant_creator', 'name', 'category', 'country', 'street', 'city', 'zip', 'website',
                  'phone', 'email', 'opening_hours',
                  'price_level', 'image', 'review_count', ]
        read_only_fields = ['id', 'restaurant_creator', 'review_count']

    def create(self, validated_data):
        return Restaurant.objects.create(
            restaurant_creator=self.context.get('request').user,
            **validated_data
        )


class ReviewsSerializer(serializers.ModelSerializer):
    likes_count = serializers.SerializerMethodField(read_only=True)

    def get_likes_count(self, reviews):
        return reviews.review_comments.count()

    class Meta:
        model = Reviews
        fields = ['owner_of_review', 'belongs_to_restaurant', 'content', 'created', 'modified', 'rating', 'likes_count']
        read_only_fields = ['owner_of_review', 'belongs_to_restaurant', 'created', 'modified', 'rating', 'likes_count']

    def create(self, validated_data):
        pk = self.context.get('view').kwargs['pk'],
        return Reviews.objects.create(
            content=self.context.get('request').POST['content'],
            rating=int(self.context.get('request').POST['rating']),
            owner_of_review=self.context.get('request').user,
            belongs_to_restaurant=Restaurant.objects.get(id=pk[0])
        )


class LikesReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = LikesReview
        fields = ['review_liked_by', 'likes_review']
        read_only_fields = ['review_liked_by', 'likes_review']
