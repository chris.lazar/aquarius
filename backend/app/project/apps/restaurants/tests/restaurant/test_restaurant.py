# from django.contrib.auth import get_user_model
# from rest_framework import status
#
# from project.apps.restaurants.tests.master_tests import MasterTestWrapper
# from project.apps.restaurants.models import Restaurant
#
# User = get_user_model()
#
#
# class GetRestaurantsTests(MasterTestWrapper.MasterTests):
#     endpoint = 'get-restaurants'
#     methods = ['GET']
#
#     def setUp(self):
#         super().setUp()
#         for i in range(10):
#             Restaurant.objects.create(
#                 restaurant_creator=self.user.user_profile,
#                 name=f'Restaurant {i}',
#                 category="hello",
#                 country="switzerland",
#                 street="langstrasse",
#                 city="zürich",
#                 zip=8004,
#                 website=f"www.resti-{i}.ch",
#                 phone=f"033-{i}",
#                 email=f"email{i}",
#                 opening_hours="8-12",
#                 price_level=2,
#                 image="www.image",
#             )
#
#     def test_get_restaurant_count(self):
#         url = self.get_url()
#         self.authorize(user=self.user)
#         response = self.client.get(url)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#         self.assertEqual(len(response.data), 10)
